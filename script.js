let input = document.getElementById("input");
let inputtwo = document.getElementById("inputtwo")
let botao = document.getElementById("botao")
let botaoLimpar = document.getElementById("botaoLimpar") 
let paragrafo = document.getElementById("paragrafo")
let armazenando = document.getElementById("armazenandoResposta")
let magreza = document.getElementById("magreza")
let pesoNormal = document.getElementById("pesoNormal")
let sobrepeso = document.getElementById("sobrepeso")
let obesidade = document.getElementById("obesidade")
let obesidadeMorbida = document.getElementById("obesidadeMorbida")
let tabela = document.getElementById("tabela")



/* Função para resetar */ 
botaoLimpar.addEventListener("click", () => {
    if(input.value === "" || inputtwo.value === "") {
        alert("Campos já estão vazios")
    }

    input.value = ""
    inputtwo.value = ""
    paragrafo.innerHTML = ""
    magreza.style.background = "white"
    pesoNormal.style.background = "white"
    sobrepeso.style.background = "white"
    obesidade.style.background = "white"
    obesidadeMorbida.style.background = "white"

})


/* Função geral */ 
botao.addEventListener("click", () => {
    if(input.value === "" && inputtwo.value === "") {
        alert("Campos vazios, Por favor, Preencha.")
    }

    armazenando.appendChild(paragrafo)
    
    const peso = input.value
    const altura = inputtwo.value
    const imc = peso / (altura * altura)
    
    
    paragrafo.style.color = "red"


    if(imc <= 18.5) {
        paragrafo.innerText = `${imc.toFixed(2)}`
        magreza.style.transition = "0.4s"
        magreza.style.background = "#c7f9cc"
       

    } else if(imc > 18.5 && imc <= 24.99) {
        paragrafo.textContent = `${imc.toFixed(2)}`
        pesoNormal.style.transition = "0.4s"
        pesoNormal.style.background = "#c7f9cc"

    } else if(imc > 24.99 && imc <= 29.99) {
        paragrafo.textContent = `${imc.toFixed(2)}`
        sobrepeso.style.transition = "0.4s"
        sobrepeso.style.background = "#c7f9cc"

    } else if(imc > 29.99 && imc <= 39.99) {
        paragrafo.textContent = `${imc.toFixed(2)}`
        obesidade.style.transition = "0.4s"
        obesidade.style.background = "#c7f9cc"

    } else if(imc > 39.99) {
        paragrafo.textContent = `${imc.toFixed(2)}`
        obesidadeMorbida.style.transition = "0.4s"
        obesidadeMorbida.style.background = "#c7f9cc"
    }
})
